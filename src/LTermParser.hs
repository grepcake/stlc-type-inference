module LTermParser (ltermP, runP, LTerm) where

import           Control.Applicative (optional, (<|>))
import           Data.Functor        (($>))

import           Lexer               (Token (..))
import           LTerm               (LTerm)
import qualified LTerm               as LT
import           Parser              (Parser (runP), chainl1, ok, single, token)

type TParser = Parser [Token]

ltermP :: TParser LTerm
ltermP = abstractionP <|> applicationP <|> atomP

abstractionP :: TParser LTerm
abstractionP = LT.L <$> (single SlashT *> token getId <* single DotT) <*> ltermP
  where getId (IdT s) = Just s
        getId _       = Nothing

applicationP :: TParser LTerm
applicationP = do
  fpart <- chainl1 atomP (ok $> LT.A)
  spart <- optional abstractionP
  return $ maybe fpart (LT.A fpart) spart

atomP :: TParser LTerm
atomP = single OpParenT *> ltermP <* single ClParenT
    <|> token getVal
  where getVal (IdT s) = Just (LT.V s)
        getVal _       = Nothing
